import React, { useState, useEffect } from "react";
import "./App.css";
const bip39 = require("bip39");
const hdkey = require("hdkey");
const createHash = require("create-hash");
const bs58check = require("bs58check");

function App() {
  const [mnemonicValue, setMnemonicValue] = useState("");
  const [mnemonicData, setMnemonicData] = useState("");
  const [privateKey, setPrivateKey] = useState("");
  const [addresss, setAddresss] = useState("");

  const mnemonic = bip39.generateMnemonic(mnemonicValue);

  const masterSeed = bip39.mnemonicToSeed(mnemonic);
  const root = hdkey.fromMasterSeed(masterSeed);
  const masterPrivateKey = root.privateKey.toString("hex");
  const addrnode = root.derive(`M/44'/0'/0'/0/0/0`);
  const step1 = addrnode._publicKey;
  const step2 = createHash("sha256").update(step1).digest();
  const step3 = createHash("rmd160").update(step2).digest();

  const step4 = Buffer.allocUnsafe(21);
  step4.writeUInt8(0x00, 0);
  step3.copy(step4, 1);
  const btcAddress = bs58check.encode(step4);
  // console.log(`mnemonic: ${mnemonic}`);
  // console.log(`private Key: ${masterPrivateKey}`);
  // console.log(`btc Address: ${btcAddress}`);

  useEffect(() => {
    setMnemonicData(mnemonic);
    setPrivateKey(masterPrivateKey);
    setAddresss(btcAddress);
  }, [mnemonicValue]);

  return (
    <div className="App">
      <h1>Generate the mnemonic here:-</h1>
      <select onChange={(e) => setMnemonicValue(e.target.value)}>
        <option value="128">128</option>
        <option value="256">256</option>
      </select>
      <h1>mnemonic Data</h1>
      <p>{mnemonicData}</p>
      {mnemonicValue && mnemonicData ? (
        <div>
          <h1>Private Key</h1>
          <p>{privateKey}</p>
          <h1>Address</h1>
          <p>{addresss}</p>
        </div>
      ) : (
        ""
      )}
    </div>
  );
}

export default App;
